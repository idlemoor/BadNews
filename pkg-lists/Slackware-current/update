#!/bin/sh
# BadNews vulnerability monitor
# Update the Slackware current package list
#-----------------------------------------------------------------------

set -eu

#-----------------------------------------------------------------------

function refresh()
{
  # Get an up-to-date download from a url
  local url="$1"
  local pathname="${2:-$(basename "$url")}"

  TIMECOND=""
  [ -f "$pathname" ] && TIMECOND="--time-cond $pathname"

  curl --disable --connect-timeout 10 --retry 2 --fail \
    --location --insecure --ciphers ALL $TIMECOND \
    --output "$pathname" "$url"
  return $?
}

#-----------------------------------------------------------------------
# Manage the Slackware GPG key.
# Get it from: http://www.slackware.com/gpg-key
# It won't expire until 2038, so we'll store it in the BadNews git
# repo, and create a keyring for it in a subdirectory.
# Unlike pkgsrc, we'll import it only if the subdirectory doesn't exist.

export GNUPGHOME=./gnupg
if [ ! -d "$GNUPGHOME" ]; then
  mkdir -m 700 "$GNUPGHOME"
  gpg --quiet --import gpg-key
fi

#-----------------------------------------------------------------------

refresh "http://slackware.uk/slackware/slackware64-current/CHECKSUMS.md5"
refresh "http://slackware.uk/slackware/slackware64-current/CHECKSUMS.md5.asc"
gpg --quiet --verify CHECKSUMS.md5.asc CHECKSUMS.md5

#-----------------------------------------------------------------------

# Output the package names and versions.
grep -v '/source/' CHECKSUMS.md5 \
  | grep '\.t.z$' \
  | sed 's:.*/::' \
  | rev \
  | cut -f3- -d- \
  | sed -e 's:-:,:' -e 's/^/,,/' \
  | rev \
  | sort \
  > list.csv

#-----------------------------------------------------------------------

exit 0
