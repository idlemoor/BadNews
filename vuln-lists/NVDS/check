#!/bin/sh
# BadNews vulnerability monitor
# Check a package list against the NIST NVD database
#
# Note that the pathname in the cve-check-tool command invocation must 
# end in '.csv', or else cve-check-tool will silently do nothing.
#
#-----------------------------------------------------------------------

set -eu

# This requires system/cve-check-tool from SlackBuilds.org
[ -x /usr/bin/cve-check-tool ] || { echo "cve-check-tool not found :(" >&2; exit 1; }

dn=$(dirname "$1")
bn=$(basename "$1")
rn="results_$(basename $(pwd))"

# we get a few more matches in lower case, so:
INPUT_CSV="/tmp/${bn}"
tr '[:upper:]' '[:lower:]' < "$1" | sort -u > "${INPUT_CSV}"

# Create the output in two formats.
OUTPUT_HTML="${dn}/${rn}.html"
OUTPUT_CSV="${dn}/${rn}.csv"

# cve-check-tool hardcodes its database to live at $HOME/NVDS (derp),
# but we can subvert that :)
export HOME=$(realpath ..)

#-----------------------------------------------------------------------

# (1) html
cve-check-tool -t faux -u /tmp/$(basename "$1") > "${OUTPUT_HTML}"
echo "Wrote ${OUTPUT_HTML}"

# (2) csv
cve-check-tool -t faux -u -c "$1" | sort > "${OUTPUT_CSV}"
echo "Wrote ${OUTPUT_CSV}"

#-----------------------------------------------------------------------

exit 0
