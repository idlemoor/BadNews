#!/usr/bin/python3
"""
BadNews vulnerability monitor
Check a package list against the pkgsrc database
"""
#-----------------------------------------------------------------------

import sys
import os
import re
import fnmatch
import csv

import braceexpand  # https://pypi.org/project/braceexpand/
import libversion   # https://pypi.org/project/libversion/

#-----------------------------------------------------------------------
# Pathnames.

vulnlistpath="pkg-vulnerabilities.txt"

if len(sys.argv) >= 2:
    pkglistpath=sys.argv[1]
else:
    sys.exit("Argument missing")

resultpath="{:s}/results_{:s}.csv".format(os.path.dirname(pkglistpath),os.path.basename(os.getcwd()))

#-----------------------------------------------------------------------
# Read the package list and store it in 'plist'.
#
# We store the package list and match the vulnerability list against it,
# instead of the other way round, mostly because the package names in
# the package list are plain strings (the vulnerability list uses globs).
# Also, the package list has a simpler structure and is likely to be
# shorter than the vulnerability list.

pkglistfile=open(pkglistpath,'r')

class Pkg:
    """
    Structure describing a single package record
    """
    def __init__(p, pkgnam, pkgver, patched, ignored):
        p.name = pkgnam
        p.version = pkgver
        p.patched = patched
        p.ignored = ignored

plist=[]
# A simple list of Pkg instances
#   (don't use a dictionary -- we don't do lookups, and there may be
#   multiple versions of a package)

recnum=0
pkgcsv=csv.reader(pkglistfile,dialect="unix",strict=True)
try:
    for pkgrec in pkgcsv:
        recnum=recnum+1
        pkgnam, pkgver, pkgpatched, pkgignored = pkgrec
        plist.append(Pkg(pkgnam,pkgver,pkgpatched,pkgignored))
except:
    print("Invalid pkglist format, line {:d}".format(recnum),file=sys.stderr)

pkglistfile.close()
print("Processed {:d} records from {:s}".format(recnum,pkglistpath))

#-----------------------------------------------------------------------
# Read and match the vulnerability list.

vulnlistfile=open(vulnlistpath,'r')
resultfile=open(resultpath,'w')

# The pkgsrc pkg-vulnerabilities.txt file has the record format
#   <vexpr><space><vcategory><space><vurl>
# where
#   <vexpr> is a conditional expression to match a package name and
#     version (see below). The values are not unique.
#   <vcategory> is a text string describing the type of vulnerability
#   <vurl> is a URL where more info can be found
#   <space> is a single space character
#
# The first field <vexpr> is one of the following:
#   <vbrace><cond1><cond2>
#   <vbrace><cond1>
#   <vbrace>
# where
#   <vbrace> is a shell-style glob possibly needing brace expansion
#   <cond> is a condition, comprising <op><version>, where
#     <op> is one of  <  <=  =  >=  >
#     <version> is a simple version string (package-specific format)
#
# Reference: http://www.netbsd.org/gallery/presentations/joerg/eurobsdcon2006/pkg_install.pdf

class Cond():
    """
    A condition that comprises an operator <op> and a version <ver>.
    """
    def __init__(c, op, version):
        c.op = op
        c.version = version

def printvulns(vglob,vcondlist,vcategory,vurl):
    """
    Check all packages 'p' in the global 'plist'
    Print a result record if
      * 'p.name' =~ 'vglob', and
      * 'p.version' 'vcond.op' 'vcond.version', for all vcond in vcondlist
    """

    # Sadly, -[0-9]* does not mean what it literally says. Oh dear no.
    # It means "any version", with, like, dots and stuff...
    # so let's get rid of it.
    if vglob.endswith("-[0-9]*"):
        vglob=vglob[0:-7]

    # Explictly compare in lower-case, because we want to be case
    # insensitive, but these aren't filenames and we don't want
    # to be at the mercy Python being "clever" about the host OS.
    reglob=re.compile(fnmatch.translate(vglob.lower()))

    for p in plist:

        matched = reglob.fullmatch(p.name.lower()) is not None
        if matched:
            vulnerable=True
            for vcond in vcondlist:
                compare = libversion.version_compare(p.version,vcond.version)
                if vcond.op == "<=":
                    matched = matched and compare <= 0
                elif vcond.op == ">=":
                    matched = matched and compare >= 0
                elif vcond.op == "<":
                    matched = matched and compare < 0
                elif vcond.op == "=":
                    matched = matched and compare == 0
                elif vcond.op == ">":
                    matched = matched and compare > 0
                else:
                    # silently drop the condition
                    continue

        if matched:
            # we have a vulnerability!
            print("{:s},{:s},{:s},{:s},{:s},{:s}".format(
                p.name, p.version, p.patched, p.ignored,
                vcategory, vurl),
                file=resultfile)


recnum=0
vulncsv=csv.reader(vulnlistfile,delimiter=" ",dialect="unix",strict=True)
try:
    for vrec in vulncsv:
        recnum=recnum+1
        vexpr, vcategory, vurl = vrec

        vexprchunks=re.split("(<=|>=|<|=|>)",vexpr)
        numchunks=len(vexprchunks)
        if numchunks == 1:
            vcondlist=[]
        elif numchunks == 3:
            vcondlist=[Cond(vexprchunks[1],vexprchunks[2])]
        elif numchunks == 5:
            vcondlist=[Cond(vexprchunks[1],vexprchunks[2]), Cond(vexprchunks[3],vexprchunks[4])]
        else:
            print("Invalid match expression, record {:d}".format(recnum),file=sys.stderr)
            continue

        for vglob in list(braceexpand.braceexpand(vexprchunks[0])):
            printvulns(vglob,vcondlist,vcategory,vurl)

except:
    print("Invalid record format, record {:d}".format(recnum),file=sys.stderr)


vulnlistfile.close()
print("Processed {:d} records from {:s}".format(recnum,vulnlistpath))

resultfile.close()
print("Wrote "+resultpath)

#-----------------------------------------------------------------------
