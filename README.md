# BadNews

BadNews is a vulnerability monitor cobbled together from bits that
were just lying around on the interwebs waiting to be used.

It checks a set of "package lists" against a set of "vulnerability
lists", and produces a set of reports.

Package lists are generated from the SlackBuilds.org repository and
from Slackware's CHECKSUMS.md5 files.

Vulnerability lists are free databases of vulnerabilities published
(and constantly updated) by NIST, by the FreeBSD project, and by the
pkgsrc project.


## Running

* Clone this repo.

* Build and install the required software. As root, run

  ./build+install

* Run everything inside the cloned repo. There are .gitignore files to
ensure that downloaded data feeds, working files and reports are not
visible to the git repo itself.  Note that the NVDS database needs
nearly 1Gb of storage.

* To manually update all the package lists and vulnerability lists, run

  ./update

* To manually check all the package lists against all the vulnerability
lists, run

  ./check

* To run the whole thing from cron (for example, daily at 10:00)

  0 10 * * * /path/to/update && /path/to/check


## Developer's notes


### Project structure

Each vulnerability list is in a subdirectory 'vuln-lists/name', which
contains:

  * a script named 'update', which creates or updates the vulnerability
    list
  * a script named 'check', which checks a supplied package list against
    the vulnerability list
  * working data (in the case of NVDS this is ~900Mb!)

Each package list is in a subdirectory 'pkg-lists/name', which contains:

  * a script named 'update', which creates or updates the package list
  * working data
  * the package list, which is always named 'list.csv'
  * the results of running each vulnerability list check , which is
    always named 'results_vlist.csv'

Individual updates and individual checks can be disabled by removing
execute permission from the relevant scripts.

The subdirectory 'slackbuilds' contains SlackBuilds for tools used to
check the vulnerability lists -- specifically, 'cve-check-tool' (from
SBo) and 'vxquery'.


### Package list format

Each package list is created (by the relevant 'update' script) as a csv
file in the format expected by cve-check-tool (which is used to check
NVDS).

The csv fields are (from reading the source):
  1. package name
     (there is no documentation about how this field is mapped to the
     names in the CVE database)
  2. version
     (but what's a "fractional" comparison?)
  3. patched
     (if this field is non-null, the package is reported as patched)
  4. ignored
     (if this field is non-null, the package is not reported)

Anything else must transform this format to its own format, in the
relevant 'check' script.


### To add a new package list

  * Create a new subdirectory in pkg-lists/
  * Provide an executable named 'update' which creates or updates a file
    named 'list.csv' in the package list format described above


### To add a new vulnerability list

  * Create a new subdirectory in vuln-lists/
  * Provide an executable named 'update' which creates or updates a
    local mirror or database of the upstream vulnerability feed
  * Provide an executable named 'check', which checks a package list
    (specified by the first command-line argument) against the local
    mirror or database, and writes a report (specified by the
    environment variable REPORTNAME)


## TODO

  * slackware-stable package list
  * installed packages on the current host
  * Debian vuln list update and check
  * Combine reports into one big report
  * Notifications
  * Documentation
  * 'patched' and 'ignored'
  * Package name aliases
